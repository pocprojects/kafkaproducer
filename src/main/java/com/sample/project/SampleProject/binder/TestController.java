package com.sample.project.SampleProject.binder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sample.project.SampleProject.methods.RabbitMQSender;
import com.sample.project.SampleProject.methods.TestBinding;

@RestController
@EnableBinding(TestBinding.class)
public class TestController {

	@Autowired
	TestBinding testBinding;
	
	@Autowired
	RabbitMQSender rabbitMQSender;	

	@GetMapping("/kafka")
	public String publish() {
		String greeting = "Message from Kafka";
		
		testBinding.test().send(MessageBuilder.withPayload(greeting).build());
		return "message Send to the kafka";
	}
	
	
	@GetMapping("/rabbit")
	public String producer() {
	System.out.println("hello");
	String rabbitTest = "Message from Rabbitmq ";
		rabbitMQSender.send(rabbitTest);

		return "Message sent to the RabbitMQ ";
	}

}
