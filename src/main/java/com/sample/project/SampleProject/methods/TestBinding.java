package com.sample.project.SampleProject.methods;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface TestBinding {
	
	@Output("testchannel")
    MessageChannel test();

}
